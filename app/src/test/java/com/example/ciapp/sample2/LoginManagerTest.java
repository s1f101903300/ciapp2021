package com.example.ciapp.sample2;


import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class LoginManagerTest {
    private LoginManager loginManager;
    private LoginManager loginManager2;
    private LoginManager loginManager3;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "password");
    }

    @Test
    public void testLoginSuccess() throws LoginFailedException {
        User user = loginManager.login("Testuser1", "password");
        assertThat(user.getUsername(), is("Testuser1"));
        assertThat(user.getPassword(), is("password"));
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginWrongPassword() throws LoginFailedException {
        User user = loginManager.login("Testuser1", "1234");
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginUnregisteredUser() throws LoginFailedException {
        User user = loginManager.login("iniad", "password");
    }

    @Test(expected = ValidateFailedException.class)
    public void testRegisterWrongUsername() throws ValidateFailedException {
        loginManager2 = new LoginManager();
        loginManager2.register("testuser1", "password");
    }

    @Test(expected = ValidateFailedException.class)
    public void testRegisterWrongPassword() throws ValidateFailedException {
        loginManager3 = new LoginManager();
        loginManager3.register("Testuser1", "1234");
    }
}